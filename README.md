#Assignment 2

**ავტვირთე მხოლოდ kt  ფაილი**

1. დაწერეთ ფუნქცია რომელიც პარამეტრად მიიღებს მთელი რიცხვების მასივს და
   დააბრუნებს რამდენი განსხვავებული ელემენტია მასში.(სორტირების გამოყენების გარეშე)
   

2. დაწერეთ ფუნქცია რომელიც პარამეტრად მიიღებს ორი მთელი რიცხვების მასივს და
   დააბრუნებს მათ თანაკვეთას
   

3. დაწერეთ ფუნქცია რომელიც პარამეტრად მიიღებს ორი მთელი რიცხვების მასივს და
   დააბრუნებს მათ გაერთიანებას
   

4. დაწერეთ ფუნქცია რომელიც პარამეტრად მიიღებს მთელი რიცხვების მასივს და
   დააბრუნებს ქვესიმრავლეს, რომელიც იქნება მოცემული მასივის საშუალო
   არითმეტიკულზე ნაკლები რიცხვებით შევსებული
   

5. დაწერეთ ფუნქცია რომელიც პარამეტრად მიიღებს მთელი რიცხვების მასივს იპოვეთ ამ
   მასივში რიგით მეორე მაქსიმალური და რიგქით მეორე მინიმალური რიცხვი.