package com.example.assignment_2

import android.util.Log.d

class Assignment2 () {

    fun getUniqueValueCount(array: List<Int>): Int{
        return(array.distinct().count())
    }
    fun getSimilarValuesFromArray(array1: List<Int>, array2: List<Int>):List<Int>{
        return array1.intersect(array2.toList()).toList()
    }
    fun combineArrays(array1: List<Int>, array2: List<Int>):List<Int>{
        return  array1.union(array2).toList()
    }
    fun getListOfLessAVG(array: List<Int>): List<Int>{
        return array.filter{it < array.average()}
    }
    fun getMaxSecond(array: List<Int>):Int{
        println(array.sorted())
        return array.sorted()[array.size-2]
    }
    fun getMinSecond(array: List<Int>): Int{
        return array.sorted()[1]
    }

}

fun main() {
    val array1 = listOf<Int>(5,10,20,12,15,15,5)
    val array2 = listOf<Int>(5,5,12,16)
    val array3 = listOf<Int>(10,9,23,12,5,2,4,1)
    val object1 = Assignment2()

    println(object1.getUniqueValueCount(array1))
    println(object1.getSimilarValuesFromArray(array1,array2))
    println(object1.combineArrays(array1,array2))
    println(object1.getListOfLessAVG(array3))
    println(object1.getMaxSecond(array3))
    println(object1.getMinSecond(array3))
}